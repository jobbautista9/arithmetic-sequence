/* This code finds any missing last term of an arithmetic sequence as long
   as the first term, n, and the common difference are known.
*/

#include <stdio.h>

int main(void) {

	float A1; // This is the first term of our arithmetic sequence
	float n; // This is the position of the last term we need to find
	float difference; // The common difference of our arithmetic sequence

	float An; // Our final answer


	// Time to input the necessary numbers
	printf("First term: ");
	scanf("%f", &A1);
	
	printf("n= ");
	scanf("%f", &n);

	printf("Common difference: ");
	scanf("%f", &difference);


	// Now we can get the nth term
	An = (A1 + (n - 1) * difference);
	printf("The answer is %f\n", An);

	return 0;
}

