/* This code finds the series of an arithmetic sequence as long as the last
   term, n, and the common difference are known.
*/

#include <stdio.h>

int main(void) {

	float A1; // This is the first term of our arithmetic sequence
	float n; // This is the position of our last term
	float difference; // The common difference of our arithmetic sequence

	float series; // Our final answer


	// Time to input the necessary numbers
	printf("First term: ");
	scanf("%f", &A1);
	
	printf("n= ");
	scanf("%f", &n);

	printf("Common difference: ");
	scanf("%f", &difference);


	// Now we can get our arithmetic series
	series = ((n / 2) * ((2 * A1) + ((n - 1) * difference)));
	printf("The answer is %f\n", series);

	return 0;
}

