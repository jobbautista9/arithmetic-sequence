/* This code finds any missing common difference of an arithmetic sequence
   as long as the last term, n, and the first term are known.
*/

#include <stdio.h>

int main(void) {

	float An; // This is the last term of our arithmetic sequence
	float n; // This is the position of the last term
	float A1; // Our first term

	float difference; // Our final answer


	// Time to input the necessary numbers
	printf("Last term: ");
	scanf("%f", &An);
	
	printf("n= ");
	scanf("%f", &n);

	printf("First term: ");
	scanf("%f", &A1);


	// Now we can get the nth term
	difference = ((An - A1) / (n - 1));
	printf("The answer is %f\n", difference);

	return 0;
}

