/* This code finds any missing first term of an arithmetic sequence as long
   as the last term, n, and the common difference are known.
*/

#include <stdio.h>

int main(void) {

	float An; // This is the last term of our arithmetic sequence
	float n; // This is the position of the last term
	float difference; // The common difference of our arithmetic sequence

	float A1; // Our final answer


	// Time to input the necessary numbers
	printf("Last term: ");
	scanf("%f", &An);
	
	printf("n= ");
	scanf("%f", &n);

	printf("Common difference: ");
	scanf("%f", &difference);


	// Now we can get the first term
	A1 = (An - ((n - 1) * difference));
	printf("The answer is %f\n", A1);

	return 0;
}

