# Creates four programs from the source files
out=A1 An difference series
default: precal
precal: $(out)

.PHONY: clean
	clean:
	rm -R out
